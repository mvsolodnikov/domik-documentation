# End Node
End node is an exit point of the [[Pipeline]].

![[Pasted image 20210105174004.png]]

- Output - cells which will be builded by House Builder.
- Override Skin Layer 1 - 

To create an **End Node** right click on the Pipeline background -> Create Node -> Flow -> End Node.