# Start Node
**Start Node** is an entry point of [[Pipeline]].
![[Pasted image 20210105173537.png]]
It constructs a set of cells based on [[House Generator]] size.
To create a **Start Node** right click on the Pipeline background -> Create Node -> Flow -> Start Node.